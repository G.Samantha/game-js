import { monster } from "./entities";
import questionMark from "../public/asset/m.svg";
import aknosome from "../public/asset/Aknosome.webp";
import almudron from "../public/asset/Almudron.webp";
import bishaten from "../public/asset/Bishaten.webp";
import chameleos from "../public/asset/Chameleos.webp";
import diablos from "../public/asset/Diablos.webp";
import espinas from "../public/asset/Espinas.webp";
import felyne from "../public/asset/Felyne.webp";
import furiousRajang from "../public/asset/Furious_Rajang.webp";
import garangolm from "../public/asset/Garangolm.webp";
import glowValstrax from "../public/asset/Glow_Valstrax.webp";
import goldRathian from "../public/asset/Gold_Rathian.webp";
import goreMagala from "../public/asset/Gore_Magala.webp";
import kushalaDaora from "../public/asset/Kushala_Daora.webp";
import lagombi from "../public/asset/Lagombi.webp";
import lucentNargacuga from "../public/asset/Lucent_Nargacuga.webp";
import lunagaron from "../public/asset/Lunagaron.webp";
import magnamalo from "../public/asset/Magnamalo.webp";
import malzeno from "../public/asset/Malzeno.webp";
import melynx from "../public/asset/Melynx.webp";
import mizutsune from "../public/asset/Mizutsune.webp";
import nargacuga from "../public/asset/Nargacuga.webp";
import pukei from "../public/asset/Pukei.webp";
import seregios from "../public/asset/Seregios.webp";
import shagaruMagala from "../public/asset/Shagaru_Magala.webp";
import silverRathalos from "../public/asset/Silver_Rathalos.webp";
import teostra from "../public/asset/Teostra.webp";
import tetranadon from "../public/asset/Tetranadon.webp";
import violetMizutsune from "../public/asset/Violet_Mizutsune.webp";
import volvidon from "../public/asset/Volvidon.webp";
import zinogre from "../public/asset/Zinogre.webp";

var monstersList = [
    {
        url: aknosome,
        name: "Aknosome",
        point: 1,
    },
    {
        url: almudron,
        name: "Almudron",
        point: 2,
    },
    {
        url: bishaten,
        name: "Bihsaten",
        point: 1,
    },
    {
        url: chameleos,
        name: "Chameleos",
        point: 5,
    },
    {
        url: diablos,
        name: "Diablos",
        point: 3,
    },
    {
        url: espinas,
        name: "Espinas",
        point: 4,
    },
    {
        url: felyne,
        name: "Felyne",
        point: 1,
    },
    {
        url: furiousRajang,
        name: "Rajang orage",
        point: 4,
    },
    {
        url: garangolm,
        name: "Garangolm",
        point: 3,
    },
    {
        url: glowValstrax,
        name: "Valstrax écarlate",
        point: 5,
    },
    {
        url: goldRathian,
        name: "Rathian d'or",
        point: 4,
    },
    {
        url: goreMagala,
        name: "Gore magala",
        point: 5,
    },
    {
        url: kushalaDaora,
        name: "Kushala Daora",
        point: 5,
    },
    {
        url: lagombi,
        name: "Lagombie",
        point: 1,
    },
    {
        url: lucentNargacuga,
        name: "Nargacuga sélénite",
        point: 4,
    },
    {
        url: lunagaron,
        name: "Lunagaron",
        point: 3,
    },
    {
        url: magnamalo,
        name: "Magnamalo",
        point: 4,
    },
    {
        url: malzeno,
        name: "Malzeno",
        point: 5,
    },
    {
        url: melynx,
        name: "Melynx",
        point: 1,
    },
    {
        url: mizutsune,
        name: "Mizutsune",
        point: 3,
    },
    {
        url: nargacuga,
        name: "Nargacuga",
        point: 3,
    },
    {
        url: pukei,
        name: "Pukei pukei",
        point: 1,
    },
    {
        url: seregios,
        name: "Seregios",
        point: 3,
    },
    {
        url: shagaruMagala,
        name: "Shagaru Magala",
        point: 5,
    },
    {
        url: silverRathalos,
        name: "Rathalos d'argent",
        point: 4,
    },
    {
        url: teostra,
        name: "Teostra",
        point: 5,
    },
    {
        url: tetranadon,
        name: "Tetranadon",
        point: 1,
    },
    {
        url: violetMizutsune,
        name: "Mizutsune mauve",
        point: 4,
    },
    {
        url: volvidon,
        name: "Voldidon",
        point: 1,
    },
    {
        url: zinogre,
        name: "Zinogre",
        point: 3,
    },
];


let selectedMonsters: Element[] = [];
let score: number = 0;
let move: number = 0;
let imgSummerize = document.querySelector<HTMLImageElement>(".img-summarize");
let monsterNameDiv = document.querySelector<HTMLElement>("#monster-name");
let monsterFind = document.querySelector<HTMLElement>(".monster-find");
let scoreElement = document.querySelector<HTMLElement>(".score");
let moveElement = document.querySelector<HTMLElement>(".move");
let resultMove = document.querySelector<HTMLElement>(".result-move");
let resultScore = document.querySelector<HTMLElement>(".result-score");
const boardContainer = document.querySelector<HTMLElement>(".board-container");
const boxStart = document.querySelector<HTMLElement>(".board-start");
const boxResult = document.querySelector<HTMLElement>(".board-result");
const boxGame = document.querySelector<HTMLElement>(".game");
const buttonRestartGame = document.querySelector<HTMLButtonElement>(".button-game-restart");
const buttonRestartResult = document.querySelector<HTMLButtonElement>(".button-result-restart");
const buttonEasy = document.querySelector<HTMLButtonElement>(".button-easy");
const buttonNormal = document.querySelector<HTMLButtonElement>(".button-normal");
const buttonHard = document.querySelector<HTMLButtonElement>(".button-hard");
const buttonVeryHard = document.querySelector<HTMLButtonElement>(".button-veryHard");

/**
 * function qui cache toutes les box
 */
function HideAllBox():void {
    if (boxStart) {
        boxStart.classList.add("hidden");
    }
    if (boxGame) {
        boxGame.classList.add("hidden");
    }
    if (boxResult) {
        boxResult.classList.add("hidden");
    }
}

/**
 * function qui montre boxStart et cache le reste
 */
function ShowStartBox(): void {
    HideAllBox();
    if (boxStart) boxStart.classList.remove("hidden");
}

/**
 * function qui montre boxGame et cache le reste
 */
function ShowGameBox(): void {
    HideAllBox();
    if (boxGame) boxGame.classList.remove("hidden");
}

/**
 * Function qui montre la result box et cache le reste
 */
function showResultBox():void {
    HideAllBox();
    if (boxResult) boxResult.classList.remove("hidden");
    if(resultMove) resultMove.innerHTML=String(move);
    if(resultScore) resultScore.innerHTML= String(score);
}

//Event sur le bouton restart de la boite Game : Reset le board et ses classes ainsi que tous les events (function reset)
buttonRestartGame?.addEventListener("click", () => {
    ShowStartBox();
    reset();
});

// Event bouton restart de la boite result: Reset le board et ses classes ainsi que tous les events (function reset)
buttonRestartResult?.addEventListener("click", () => {
    ShowStartBox();
    reset();
});

//Event sur le bouton easy: Reset le board montre la box game et donne la classe voulu à l'élément boardContainer
buttonEasy?.addEventListener("click", () => {
    resetBoardContainer();
    ShowGameBox();
    if (boardContainer) boardContainer.classList.add("board-container16"); // on ajoute la class correspondante
    generateBoard(8);
});

//Event sur le bouton normal: Reset le board montre la box game et donne la bonne classe à boardContainer
buttonNormal?.addEventListener("click", () => {
    resetBoardContainer();
    ShowGameBox();
    if (boardContainer) boardContainer.classList.add("board-container28");
    generateBoard(14);
});

//Event sur le bouton hard: Reset le board montre la box game et donne la bonne classe à boardContainer
buttonHard?.addEventListener("click", () => {
    resetBoardContainer();
    ShowGameBox();
    if (boardContainer) boardContainer.classList.add("board-container36");
    generateBoard(18);
});

//Event sur le bouton veryhard: Reset le board montre la box game et donne la bonne classe à boardContainer
buttonVeryHard?.addEventListener("click", () => {
    resetBoardContainer();
    ShowGameBox();
    if (boardContainer) boardContainer.classList.add("board-container60");
    generateBoard(30);
});

/**
 * fonction remet le plateau de jeu dans son état initial
 */
function resetBoardContainer() {
    if (boardContainer) boardContainer.innerHTML = "";
    boardContainer?.classList.remove(
        "board-container16",
        "board-container28",
        "board-container36",
        "board-container60"
    );
    boardContainer?.classList.add("board-container");
    selectedMonsters = [];
}

/**
 * fonction qui réinitalise le score, les coups et l'écran info-game
 */
function reset() {
    // on reset le score
    score = 0;
    // on reset le move
    move = 0;
    if (scoreElement) {
        scoreElement.innerHTML = String(score);
    }
    if (moveElement) {
        moveElement.innerHTML = String(move);
    }
    if (monsterFind) {
        monsterFind.classList.add('hidden');
    }
}

/**
 * function qui créer une card à partir d'un monstre et l'ajoute au board-game
 * @param monsterObject 
 */
function createCard(monsterObject: monster) {
    const divCard = document.createElement("div");
    divCard.classList.add("card");
    divCard.dataset.monsterName = monsterObject.name;
    divCard.dataset.point = String(monsterObject.point);

    const divCardContainer = document.createElement("div");
    divCardContainer.classList.add("card-container");
    divCard.appendChild(divCardContainer);

    const cardBack = document.createElement("div");
    cardBack.classList.add("card-back");
    divCardContainer.appendChild(cardBack);

    const imgB = document.createElement("img");
    imgB.classList.add("img-back");
    imgB.src = monsterObject.url;
    cardBack.appendChild(imgB);

    const cardFront = document.createElement("div");
    cardFront.classList.add("card-front");
    divCardContainer.appendChild(cardFront);

    const imgF = document.createElement("img");
    imgF.classList.add("img-front");
    imgF.src = questionMark;
    cardFront.appendChild(imgF);

    if (boardContainer) {
        // V1: Lors de la création d'une card, on l'ajoute aléatoirement au board soit en premier soit en dernier
        // const random = Math.floor(Math.random() * 2); //random un nombre entre 0 et 1
        // if (boardContainer.childNodes.length <= 0 || random === 0) {
        //     //On vérifie le nombre d'enfant de board est inférieur ou égale à 0 OU si le nombre random est égale à 0
        //     boardContainer?.appendChild(divCard); // Si oui on ajoute à la fin de l'élement board
        // } else {
        //     boardContainer?.insertBefore(divCard, boardContainer.firstChild); // sinon on l'ajoute avant le premier enfant de l'élément board
        // }

        // V2 Le mélange du tableau est fait avant createcard, on l'ajoute juste au board 
        boardContainer?.appendChild(divCard);
    }
}

/**
 * function qui retourne d'un tableau de longueur num contenant le nombre de monstre voulu à partir d'un tableau
 * @param {monster[]} arr - tableau de monstre
 * @param {number} num - Nombre de monstre voulu
 * @returns {monster[]}
 */
function getMultipleMonsters(arr: monster[], num: number) {
    return arr.slice(0, num);
}

/**
 * function qui génère le plateau de jeu
 * @param {number} numberOfMonsters - Nombre de monstre différent
 */
function generateBoard(numberOfMonsters: number):void {
    monstersList = shuffle(monstersList);
    const randomMonsters = getMultipleMonsters(monstersList, numberOfMonsters); // on créer une variable qui prend le retour function getMulitpleMonster()
    // On créer un tableau de card, on met deux fois le tableau randomMonsters pour l'avoir en double car les monstres sont sur deux cards...
    let randomCards = [...randomMonsters, ...randomMonsters];
    // ... puis on mélange le tableau
    randomCards = shuffle(randomCards);
    for (let index = 0; index < randomCards.length; index++) {
        createCard(randomCards[index]);
    }
    resetEvents();
}

/**
 * function qui rend non cliquable les cards
 */
function disebleAllCards():void {
    var cards = document.querySelectorAll(".card");
    for (let i = 0; i < cards.length; i++) {
        cards[i].classList.add("unclick");
    }
}

/**
 * function qui vérifie si les deux cards du tableau cardElements ont la même classe
 * @param {Element[]} cardElements - Tableau de deux cards
 * @returns {boolean}
 */
function cardAreMatching(cardElements: Element[]) {
    return (
        cardElements[0].dataset.monsterName === cardElements[1].dataset.monsterName
    );
    // V1 : 
    //   if (cardNames[0]===cardNames[1])
    //     return true;
    // }
    // return false;
}

/**
 * function qui remet les cards qui ne sont pas trouvé cliquable
 */
function resetClick():void {
    var cards = document.querySelectorAll(".card");
    for (let i = 0; i < cards.length; i++) {
        if (!cards[i].classList.contains("found")) {
            cards[i].classList.remove("unclick");
        }
    }
}

/**
 * function met à jour l'image et le nom de l'image trouvé
 * @param {string} monsterimg - Source de l'image
 * @param {string} monstername - Nom du monstre
 */
function updateMonsterFound(monsterimg: string, monstername: string):void {
    if (imgSummerize) imgSummerize.src = monsterimg;
    if (monsterNameDiv) monsterNameDiv.innerHTML = monstername;
    if (monsterFind) monsterFind.classList.remove('hidden');
}

/**
 * function qui réinitialise tous les évènements clicks sur les cards
 */
function resetEvents():void {
    var cards = document.querySelectorAll(".card");
    for (let i = 0; i < cards.length; i++) {
        cards[i].addEventListener("click", function () {
            if (!cards[i].classList.contains("unclick")) {
                cards[i].classList.toggle("is-flipped");
                cards[i].classList.add("unclick", "current");
                selectedMonsters.push(cards[i]);

                if (selectedMonsters.length === 2) {
                    disebleAllCards();
                    move++;
                    if (moveElement) { moveElement.innerHTML = String(move); }
                    if (cardAreMatching(selectedMonsters)) {
                        let scoreToAdd = 0;
                        for (let index = 0; index < selectedMonsters.length; index++) {
                            selectedMonsters[index].classList.add("found");
                            selectedMonsters[index].classList.remove("current");
                            scoreToAdd = +selectedMonsters[index].dataset.point;
                        }
                        score += scoreToAdd;
                        const url = selectedMonsters[0].querySelector('.img-back').src;
                        updateMonsterFound(url, selectedMonsters[0].dataset.monsterName);
                        if (scoreElement) { scoreElement.innerHTML = String(score); }
                        const cardLength = document.querySelectorAll<HTMLElement>(".card").length;
                        const foundLength = document.querySelectorAll<HTMLElement>(".found").length;

                        if (cardLength === foundLength) {
                            if (buttonRestartGame) buttonRestartGame.disabled = true;
                            setTimeout(() => {  //setTimeout me permet de ralentir l'execution de ce qu'il contient
                                showResultBox();
                                if (buttonRestartGame) buttonRestartGame.disabled = false;
                            }, 1500);
                        } else {
                            resetClick();
                            selectedMonsters = [];
                        }
                    } else {
                        setTimeout(() => {  //setTimeout me permet de ralentir l'execution de ce qu'il contient
                            for (let index = 0; index < selectedMonsters.length; index++) {
                                selectedMonsters[index].classList.remove(
                                    "is-flipped",
                                    "current"
                                );
                            }
                            resetClick();
                            selectedMonsters = [];
                        }, 1500);
                    }
                }
            }
        });
    }
}


/**
 * function qui mélange le tableau de monstre 
 * (trouvée sur internet, adaptée à mes besoins)
 * @param {monster[]} array - tableau de monstre à mélanger
 * @returns {monster[]}
 */
function shuffle(array:monster[]) {
    let currentIndex = array.length,  randomIndex;
  
    // tant qu'on n'a pas traité chaque élement...
    while (currentIndex != 0) {
  
      //... on choisi l'index aléatoirement d'un autre élement...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;
  
      //... et on échange leurs places.
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }
  
    return array;
  }