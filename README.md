# Projet Jeu JS

Application web Memory Game

# Intro

Pour ce troisème projet j'ai décidé de créer un memory game avec JavaScript (typescript). Le but est d'utilisé l'algorithmie de base, la manipulation du DOM et la POO, sans framework.

# Organisation

J'ai commencé à détailler mon projet:
- Découper chaque partie en notant toutes les fonctions dont j'aurai besoin.
- Noter les parties avec leurs besoins (exemple: Ma partie animation de carte est-elle faite avec le CSS ou le JS ?)
- Mettre dans l'ordre d'importance (l'animation de carte est moins importante que l'affichage recto verso d'une carte)
- Commenter et créer le JSDoc

 J'ai décidé de générer un compteur de point, un compteur de coups et d'une randomisation du plateau d'image.
 Le plateau de jeu est généré par Typescript, les cartes aussi, et les données de chaque monstre (qui sont les cartes à trouver) sont dans un tableau d'objet.
 Une interface à été créé ainsi que des imports pour les images.

# Notes

Le responsive étant compliqué pour ce type de jeu, je n'ai pas pris le temps de me pencher dessus, mais cela pourrait être un petit défi.

# Information

- Maquette: https://www.figma.com/file/9z2mKwr3xbFbjvTKOwqdoC/memory-game-JS?node-id=0%3A1&t=8GtqbHTSfnOQZDvz-0
- Lien du site: https://memonster.netlify.app/
- Lien du git : https://gitlab.com/G.Samantha/game-js


